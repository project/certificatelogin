<?php

namespace Drupal\certificatelogin\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\certificatelogin\Service\LoginRegisterService;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\Html;

/**
 * Class CertificateLoginController.
 *
 * @package Drupal\certificatelogin\Controller
 */
class CertificateLoginController extends ControllerBase {

  /**
   * The login and registration service.
   */
  protected $loginRegisterService;

  /**
   * Constructs a new CertificateLoginController object.
   */
  public function __construct(LoginRegisterService $loginRegisterService) {
    $this->loginRegisterService = $loginRegisterService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('certificatelogin.login_register')
    );
  }

  /**
   * Main controller method, used by `/certificatelogin/login` route.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function processLoginRequest() {
    $confirm = Html::escape(\Drupal::request()->query->get('confirm'));

    # Call into the service to attempt to authenticate and login,
    $msg = $this->loginRegisterService->processLoginRequest();
    if ($msg) {
      return $this->displayErrorAndResetPage($this->t($msg));
    } else {
      # Check if the user exists, and if so log them in.
      if ($this->loginRegisterService->doesUserExist()) {
        $this->loginRegisterService->loginAccount();
      } else {
        # If not, and admin has configured to confirm user registration, ask for confirmation.
        if (!$confirm && $this->loginRegisterService->confirmRegistration()) {
          return $this->redirect('certificatelogin.confirm');
        } else {
          $this->loginRegisterService->registerAccount();
        }
      }
    }

    # No error message, just return to the user login page.
    return $this->redirect('user.page');
  }

  protected function displayErrorAndResetPage($message) {
    $this->messenger()->addError($message);

    # First check for a destination parameter.
    $destination = \Drupal::destination()->get();
    if (!empty($destination) && $destination !== '/certificatelogin/login') {
      return new RedirectResponse(Url::fromUserInput($destination)->toString(), 302);
    }

    return $this->redirect('<front>');
  }
}
