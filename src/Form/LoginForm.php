<?php

namespace Drupal\certificatelogin\Form;

use Drupal\user\Form\UserLoginForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RenderInterface;
use Drupal\user\UserInterface;
use Drupal\certificatelogin\Service\LoginRegisterService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class LoginForm implements a two-stage login form to replace /user/login,
 * that allows the user to first enter a username, and on a second step, login
 * with a certificate OR a password.
 *
 * @package Drupal\certificatelogin\Form
 */
class LoginForm extends UserLoginForm {

  /**
   * The login and registration service.
   */
  protected $loginRegisterService;

  /**
   * Simple multi-step form.
   */
  protected $step = 1;

  /**
   * Constructs a new CertificateLoginController object.
   */
  public function __construct(LoginRegisterService $loginRegisterService) {
    $this->loginRegisterService = $loginRegisterService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('certificatelogin.login_register')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'certificatelogin_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $site_name = $this->config('system.site')->get('name');
    $config = $this->config('certificatelogin.settings');

    if (!$config->get('enable_two_step_login')) {
      $this->step = 2;
    }

    switch ($this->step) {
      case 1:
        $form['name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Username'),
          '#size' => 60,
          '#maxlength' => UserInterface::USERNAME_MAX_LENGTH,
          '#description' => $this->t('Enter your @s username.', ['@s' => $site_name]),
          '#required' => TRUE,
          '#attributes' => [
            'autocorrect' => 'none',
            'autocapitalize' => 'none',
            'spellcheck' => 'false',
            'autofocus' => 'autofocus',
          ],
        ];
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = ['#type' => 'submit', '#value' => $this->t('Login')];
        break;
      case 2:
        if ($config->get('allow_password_login')) {
          $name = $form_state->getValue('name');
          $form['name'] = [
            '#type' => 'value',
            '#value' => $name,
          ];
          $form['pass'] = [
            '#type' => 'password',
            '#title' => $this->t('Password'),
            '#size' => 60,
            '#description' => $this->t('Enter the password that accompanies your username OR select Login with Certificate.'),
            '#required' => FALSE,
            '#attributes' => [
              'autocorrect' => 'none',
              'autocapitalize' => 'none',
              'spellcheck' => 'false',
              'autofocus' => 'autofocus',
            ],
          ];

          $form['actions'] = ['#type' => 'actions'];
          $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Login with Password')
          ];
        }
        $form['actions']['certificate'] = [
          '#value' => $this->t('Login with Certificate'),
          '#type' => 'button',
          '#url' => Url::fromRoute('certificatelogin.login', [], array('query' => ['name' => $name]))
        ];

        break;
    }

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    switch ($this->step) {
      case 1:
        # TODO: check this corresponds to a real user account (parent method does not)
        $this->validateName($form, $form_state);
        # TODO: if not an existing user, ask if they want to register
        $form_state->setRebuild();
        $this->step++;
        break;
      case 2:
        $this->validateAuthentication($form, $form_state);
        if ($form_state->getValue('uid') != NULL) {
          $this->validateFinal($form, $form_state);
        }
    }
    return TRUE;
  }

  /**
   * If we gave a choice to use a password, check for that
   * and login normally. If not, check certificate authentication.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateAuthentication(&$form, $form_state) {
    # TODO: check which button was clicked? Or maybe just use one?
    $pass = $form_state->getValue('pass');
    $uid = $form_state->getValue('uid');

    # If they gave a password, use that to authenticate.
    if (!empty($pass)) {
      parent::validateAuthentication($form, $form_state);
    # Otherwise check for a certificate, and use that.
    } else {
      # TODO: handle 2-step login better, so we can split login and register functions.
      # For now, redirect to main controller path, which now calls into our service.
      (new RedirectResponse('/certificatelogin/login'))->send();
    }
  }
}
