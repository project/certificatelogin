<?php

namespace Drupal\certificatelogin\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\externalauth\Authmap;
use Drupal\externalauth\ExternalAuth;
use Drupal\certificatelogin\Plugin\CaSignatureVerificationPluginManager;

class LoginRegisterService implements ContainerInjectionInterface {
  const AUTHENTICATION_PROVIDER_NAME = 'certificatelogin';
  const AUTHENTICATION_NAME_DELIMITER = '|';

  /**
   * Config Factory.
   */
  protected $config;

  /**
   * External Authentication's map between local users and service users.
   *
   * @var \Drupal\externalauth\Authmap
   */
  protected $authmap;

  /**
   * External Authentication's service for authenticating users.
   *
   * @var \Drupal\externalauth\ExternalAuth
   */
  protected $externalauth;

  /**
   * The plugin manager for CA certificate verification.
   *
   * @var \Drupal\certificatelogin\Plugin\CaSignatureVerificationPluginManager
   */
  protected $caCertificateVerificationService;

  /**
   * @var The certificate provided by the user, raw from the server variables.
   */
  protected $certificate;

  /**
   * @var The parsed certificate data.
   */
  protected $certificate_info;

  public function __construct($config, Authmap $external_authmap, ExternalAuth $externalauth_externalauth, CaSignatureVerificationPluginManager $ca_certificate_verification_service) {
    $this->config = $config;
    $this->authmap = $external_authmap;
    $this->externalauth = $externalauth_externalauth;
    $this->caCertificateVerificationService = $ca_certificate_verification_service;

    $this->getClientCertificate();
    $this->getClientCertificateInfo();
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('externalauth.authmap'),
      $container->get('externalauth.externalauth'),
      $container->get('plugin.manager.certificatelogin.ca_signature_verification'),
    );
  }

  /**
   * Process a request to login with a certificate:
   *  * check if the user appears Human
   *  * a certificate is present in the request
   *  * the certificate authenticates (including signature check if configured)
   *  * DO NOT login the user, let calling party decide to login or register.
   */
  public function processLoginRequest($name = '') {
    if (!$this->currentUserIsHuman()) {
      return 'Certificate logins are only available to humans.';
    }

    if (!$client_certificate = $this->getClientCertificate()) {
      return 'No client certificate found for logging in.';
    }

    if (!$this->clientCertificateAuthenticates()) {
      return 'Your client certificate cannot be used to authenticate to this site.';
    }

    return '';
  }

  /**
   * If user is already mapped, return their user id
   * @return bool|int
   */
  public function doesUserExist() {
    return $this->authmap->getUid(
      $this->getAuthenticationUserName(),
      $this->getAuthenticationProviderName()
    );
  }

  /**
   * @return boolean state of Confirm new registration setting.
   */
  public function confirmRegistration() {
    return $this->config->get('confirm_new_registration');
  }

  /**
   * TODO: Determine if the current user is human.
   *
   * It's a good idea to check for this so we don't waste resources on bots.
   *
   * @return boolean
   *   TRUE if the current user is human; FALSE otherwise.
   *
   * @todo Implement this, possibly using Antibot's JS test.
   */
  protected function currentUserIsHuman() {
    return TRUE;
  }

  /**
   * Check for the client certificate on the request, by looking up
   * configured ENV variables to look for, and return the first one that has a value.
   * @return mixed|string
   */
  public function getClientCertificate() {
    # Only need to look this up once per request, at most.
    if (!isset($this->certificate)) {
      # Look up the server variables from which to find client certificate.
      $server = \Drupal::request()->server;
      $server_variables = $this->getClientCertificateVariables();

      # Look for a value in any of the list of server environment variables.
      foreach ($server_variables as $option) {
        $value = $server->get($option);
        if (isset($value)) {
          $this->certificate = $value;
        }
      }
      if (!isset($this->certificate)) {
        $this->certificate = ''; # Make sure it's set to something.
      }
    }
    return $this->certificate;
  }

  public function getClientCertificateInfo() {
    if (empty($this->getClientCertificate())) {
      $this->certificate_info = [];
    }

    if (!isset($this->certificate_info) || !is_array($this->certificate_info)) {
      $this->certificate_info = openssl_x509_parse($this->certificate, FALSE);
    }
    return $this->certificate_info;
  }

  /**
   * Look up the variables admin has configured to reference, for a certificate.
   * @return array|false|string[]
   */
  protected function getClientCertificateVariables()
  {
    $config = $this->config->get('certificatelogin.settings');
    return preg_split("/[\r\n]+/", $config->get('client_certificate_server_variable'));
  }

  /**
   * Determines if the client certificate can be used for authentication.
   *
   * We only accept certificates signed by the CA whose certificate was provided
   * in the module's settings. However, if a CA wasn't provided, we'll accept
   * any certificate as valid authentication.
   *
   * @param x509cert $client_certificate
   *   The client certificate provided by the user on attempting to log in.
   *
   * @return boolean
   *   TRUE if the certificate authenticates; FALSE otherwise.
   */
  protected function clientCertificateAuthenticates() {
    if (!$ca_certificate = $this->config->get('certificatelogin.settings')->get('ca_certificate')) {
      return TRUE;
    }

    if ($this->clientCertificateSignedByAuthority($this->certificate, $ca_certificate)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Log the user in, creating the account if it doesn't exist yet.
   *
   * @param x509cert $client_certificate
   *   The client certificate.
   *
   * @return \Drupal\user\UserInterface
   *   The registered Drupal user.
   */
  protected function loginUserWithCertificate() {

    if (!$this->authmap->getUid(
      $this->getAuthenticationUserName(),
      $this->getAuthenticationProviderName()
    )) {
      return $this->registerAccount();
    }
    return $this->loginAccount();
  }

  public function loginAccount() {
    return $this->externalauth->login(
      $this->getAuthenticationUserName(),
      $this->getAuthenticationProviderName()
    );
  }

  public function registerAccount() {
    return $this->externalauth->loginRegister(
      $this->getAuthenticationUserName(),
      $this->getAuthenticationProviderName(),
      $this->getUserAccountData(),
      $this->getClientCertificate()
    );
  }

  /**
   * Identify the authentication provider in the AuthMap table.
   *
   * @return string
   */
  public static function getAuthenticationProviderName() {
    return self::AUTHENTICATION_PROVIDER_NAME;
  }

  /**
   * @param $certificate_info
   * @return string
   */
  protected function getAuthenticationUserName() {
    $certificate_info = $this->getClientCertificateInfo();

    # TODO: Switch to using a fingerprint, once we have an update hook in place.
    #$authname = openssl_x509_fingerprint($certificate_info, "sha256");

    # This has overflow problems :(
    $issuer_common_name = $certificate_info['issuer']['commonName'];
    $distinguished_name = $certificate_info['name'];
    return $issuer_common_name . $this->getAuthenticationNameDelimiter() . $distinguished_name;
  }

  /**
   * Determine if a client certificate has been signed by a particular CA.
   *
   * @param x509cert $client_certificate
   *   The client certificate.
   * @param x509cert $ca_certificate
   *   The certification authority (CA) certificate.
   *
   * @return boolean
   *   TRUE if the client certificate was signed by the CA; FALSE otherwise.
   */
  protected function clientCertificateSignedByAuthority($client_certificate, $ca_certificate) {
    $plugin_id = $this->config->get('certificatelogin.settings')->get('ca_signature_verifier');
    $signature_verifier = $this->caCertificateVerificationService->createInstance($plugin_id);
    return $signature_verifier->clientCertificateSignedByAuthority($client_certificate, $ca_certificate);
  }

  protected function getUserAccountData() {
    $certificate_info = $this->getClientCertificateInfo();

    $data = [
      'name' => $certificate_info['subject']['commonName'],
    ];

    # Try to set email address by emailAdress field, then userId, and fallback to CN
    if (isset($certificate_info['subject']['emailAddress'])) {
      $data['mail'] = $certificate_info['subject']['emailAddress'];
    } elseif (isset($certificate_info['subject']['emailAddress'])) {
      $data['mail'] = $certificate_info['subject']['userId'];
    } else {
      $data['mail'] = $certificate_info['subject']['commonName'];
    }

    return $data;
  }

  public static function getAuthenticationNameDelimiter() {
    return self::AUTHENTICATION_NAME_DELIMITER;
  }
}
